package tues1309;
import java.util.*;
public class program1 {
    public static void main(String args[]) {
        List<Integer> myList = Arrays.asList(10, 15, 8, 49, 25, 98, 32);
        myList.stream()
                .filter(n -> n % 2 == 0)
                .forEach(System.out::println);

    }
}
